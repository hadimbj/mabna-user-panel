module.exports = {
  purge: {
    enabled: process.env.TAILWIND_PURGE === 'true',
    content: [
      './src/*/.{html,ts}',
    ]
  },
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      outline: false
    },
  },
  variants: {},
  plugins: [],
  corePlugins: {
    // for browser compatibility
    // https://tailwindcss.com/docs/configuration#core-plugins
    preflight: true
  }
}
