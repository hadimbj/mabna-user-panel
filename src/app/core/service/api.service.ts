import { Injectable } from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders, HttpParams} from "@angular/common/http";
import {Observable, throwError} from "rxjs";
import {catchError} from "rxjs/operators";
import {TokenService} from "../../authentication/service/token.service";
import {HttpParamsObject} from "../model/HttpParamsObject.type";

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  private baseApiUrl: string = 'http://localhost:3000/';

  constructor(
    private http: HttpClient,
    private tokenService: TokenService
  ) {
  }

  get<T>(apiControllerUrl: string, parameters: HttpParamsObject = {}, responseType?: 'json'): Observable<T> {
    const params = new HttpParams({fromObject: parameters});
    const options = {params, responseType, headers: this.getAuthenticationHeader()}

    return this.http.get<T>(this.baseApiUrl + apiControllerUrl, options)
      .pipe(
        catchError(err => ApiService.handleError(err))
      );
  }

  post<T>(apiControllerUrl: string, body?: object, parameters: HttpParamsObject = {}, responseType?: 'json'): Observable<T> {
    const params = new HttpParams({fromObject: parameters});
    const options = {params, responseType, headers: this.getAuthenticationHeader()}

    return this.http.post<T>(this.baseApiUrl + apiControllerUrl, body, options)
      .pipe(
        catchError(err => ApiService.handleError(err))
      );
  }

  put<T>(apiControllerUrl: string, body?: object, parameters: HttpParamsObject = {}): Observable<T> {
    const params = new HttpParams({fromObject: parameters});
    const options = {params, headers: this.getAuthenticationHeader()}

    return this.http.put<T>(this.baseApiUrl + apiControllerUrl, body, options)
      .pipe(
        catchError(err => ApiService.handleError(err))
      );
  }

  delete<T>(apiControllerUrl: string, parameters: HttpParamsObject = {}): Observable<T> {
    const params = new HttpParams({fromObject: parameters});
    const options = {params, headers: this.getAuthenticationHeader()}

    return this.http.delete<T>(this.baseApiUrl + apiControllerUrl, options)
      .pipe(
        catchError(err => ApiService.handleError(err))
      );
  }

  private static handleError(error: HttpErrorResponse): Observable<never> {
    // TODO: handle with error handler service
    return throwError(error);
  }

  private getAuthenticationHeader(): HttpHeaders {
    let httpHeaders = new HttpHeaders();
    const token = this.tokenService.getToken();
    if (token) {
      httpHeaders = httpHeaders.append('Authorization', 'Bearer ' + token);
    }
    return httpHeaders;
  }


}
