import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AuthenticationService} from "./authentication/service/authentication.service";
import {AuthenticationGuard} from "./authentication/authentication-guard.service";
import {AuthorizationGuard} from "./authentication/authorization-guard.service";

const routes: Routes = [
  {path: '', pathMatch: 'full', redirectTo: 'login'},
  {
    path: 'back-office',
    canActivate: [AuthenticationGuard, AuthorizationGuard],
    loadChildren: () => import('./back-office/back-office.module').then(m => m.BackOfficeModule)
  },
  {
    path: 'tasks',
    canActivate: [AuthenticationGuard],
    loadChildren: () => import('./task-management/task-management.module').then(m => m.TaskManagementModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
