import {Component, Inject, OnInit} from '@angular/core';
import {Task} from "../model/task";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {TaskService} from "../service/task.service";

@Component({
  selector: 'app-task-dialog',
  templateUrl: './task-dialog.component.html',
  styleUrls: ['./task-dialog.component.scss']
})
export class TaskDialogComponent implements OnInit {
  task: Task;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private matDialogRef: MatDialogRef<TaskDialogComponent>,
    private taskService: TaskService
  ) {
    this.task = {...data.task};
  }

  ngOnInit(): void {
  }

  update() {
    this.taskService.update(this.task).subscribe(() =>
      this.matDialogRef.close()
    );
  }

  create() {
    this.taskService.create(this.task).subscribe(() =>
      this.matDialogRef.close()
    );
  }
}
