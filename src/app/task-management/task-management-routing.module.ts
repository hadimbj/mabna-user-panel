import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {TaskListComponent} from "./task-list/task-list.component";
import {TaskManagementComponent} from "./task-management/task-management.component";

const routes: Routes = [
  {
    path: '', component: TaskManagementComponent,
    children: [
      {path: '', component: TaskListComponent},
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TaskManagementRoutingModule {
}
