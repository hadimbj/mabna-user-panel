import {Injectable} from '@angular/core';
import {ApiService} from "../../core/service/api.service";
import {Observable} from "rxjs";
import {Task} from "../model/task";

@Injectable({
  providedIn: 'root'
})
export class TaskService {

  constructor(private api: ApiService) {
  }

  getUserTasks(userId: string): Observable<Task[]> {
    return this.api.get(`users/${userId}/tasks`);

  }

  update(task: Task): Observable<Task[]> {
    return this.api.put(`tasks/${task.id}`, task);
  }

  create(task: any): Observable<Task[]> {
    return this.api.post(`tasks`, task);
  }

  remove(id: string): Observable<boolean> {
    return this.api.delete(`tasks/${id}`);
  }
}
