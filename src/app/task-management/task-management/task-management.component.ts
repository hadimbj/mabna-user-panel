import {Component, HostListener, OnInit} from '@angular/core';
import {AuthenticationService} from "../../authentication/service/authentication.service";
import {Router} from "@angular/router";
import {User} from "../../authentication/model/user";

@Component({
  selector: 'app-task-management',
  templateUrl: './task-management.component.html',
  styleUrls: ['./task-management.component.scss']
})
export class TaskManagementComponent implements OnInit {
  isMenuVisible = true;
  user?: User;
  isMobile: boolean;

  @HostListener('window:resize', ['$event'])
  sizeChange() {
    this.isMobile = window.innerWidth < 460;
  }

  constructor(
    private authenticationService: AuthenticationService,
    private router: Router
  ) {
    this.isMobile = window.innerWidth < 460;
    this.authenticationService.user$.subscribe(user => this.user = user);
  }

  ngOnInit(): void {
  }

  onHideMenu() {

  }

  toggleMenu() {
    this.isMenuVisible = !this.isMenuVisible;
  }

  logout() {
    this.authenticationService.logoutUser();
    this.router.navigateByUrl('/')
  }
}
