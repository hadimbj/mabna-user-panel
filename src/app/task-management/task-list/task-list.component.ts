import {Component, OnInit} from '@angular/core';
import {AuthenticationService} from "../../authentication/service/authentication.service";
import {filter, switchMap, tap} from "rxjs/operators";
import {TaskService} from "../service/task.service";
import {Observable} from "rxjs";
import {Task} from "../model/task";
import {MatDialog} from "@angular/material/dialog";
import {TaskDialogComponent} from "../task-dialog/task-dialog.component";
import {User} from "../../authentication/model/user";
import {MatSnackBar} from "@angular/material/snack-bar";

@Component({
  selector: 'app-task-list',
  templateUrl: './task-list.component.html',
  styleUrls: ['./task-list.component.scss']
})
export class TaskListComponent implements OnInit {
  taskList$: Observable<Task[]>;
  displayedColumns: string[] = ['id', 'title', 'isDone', 'operation'];
  private user?: User;


  constructor(
    private authenticationService: AuthenticationService,
    private taskService: TaskService,
    private matDialog: MatDialog,
    private snackBar: MatSnackBar
  ) {
    this.taskList$ = this.getTasks();
  }

  ngOnInit(): void {
  }

  editTask(task: Task) {
    this.matDialog.open(TaskDialogComponent, {data: {task}}).afterClosed().subscribe(() =>
      this.taskList$ = this.getTasks()
    );
  }

  private getTasks(): Observable<Task[]> {
    return this.authenticationService.user$.pipe(
      filter(user => !!user),
      tap(user => this.user = user),
      switchMap((user) => this.taskService.getUserTasks(user.id))
    );
  }

  createTask(): void {
    if (!this.user?.id) return;
    this.matDialog.open(TaskDialogComponent, {
      data: {
        task: {
          isDone: false,
          userId: this.user?.id
        }
      }
    }).afterClosed().subscribe(() =>
      this.taskList$ = this.getTasks()
    );
  }

  removeTask(task: Task): void {
    this.taskService.remove(task.id).subscribe(() => {
      this.snackBar.open(`Task with id = ${task.id} was removed`);
      this.taskList$ = this.getTasks()
    });
  }
}
