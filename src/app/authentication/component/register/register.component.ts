import {Component, OnDestroy} from '@angular/core';
import {Subscription} from "rxjs";
import {FormBuilder, Validators} from "@angular/forms";
import {AuthenticationService} from "../../service/authentication.service";
import {User} from "../../model/user";
import {MatSnackBar} from "@angular/material/snack-bar";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnDestroy {

  subs = new Subscription();
  registerUserForm = this.fb.group({
    name: ['', Validators.required],
    email: ['', Validators.required],
    password: ['', Validators.required],
    phoneNumber: ['', Validators.required],
  })

  constructor(
    private fb: FormBuilder,
    private authenticationService: AuthenticationService,
    private snackBar: MatSnackBar
  ) {
  }

  onSubmitForm() {
    const getFormFieldValue = (formField: string) => this.registerUserForm.get(formField)?.value;
    const user: Partial<User> = {
      name: getFormFieldValue('name'),
      email: getFormFieldValue('email'),
      password: getFormFieldValue('password'),
      phoneNumber: getFormFieldValue('phoneNumber'),
    }
    this.subs.add(this.authenticationService.registerUser(user).subscribe(
      () => this.snackBar.open('Login was successful. Welcome.'),
      () => this.snackBar.open('Username and/or Password is wrong. Please try again.')
    ));
  }

  ngOnDestroy(): void {
    this.subs.unsubscribe();
  }
}
