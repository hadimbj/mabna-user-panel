import {Component, OnDestroy} from '@angular/core';
import {FormBuilder, Validators} from "@angular/forms";
import {AuthenticationService} from "../../service/authentication.service";
import {Subscription} from "rxjs";
import {MatSnackBar} from "@angular/material/snack-bar";
import {Router} from "@angular/router";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnDestroy {

  subs = new Subscription();
  loginForm = this.fb.group({
    email: ['', Validators.required],
    password: ['', Validators.required]
  })

  constructor(
    private fb: FormBuilder,
    private authenticationService: AuthenticationService,
    private snackBar: MatSnackBar,
    private router: Router
  ) {
  }

  onSubmitForm() {
    const getFormFieldValue = (formField: string) => this.loginForm.get(formField)?.value;
    this.subs.add(this.authenticationService.LoginUser(getFormFieldValue('email'), getFormFieldValue('password')).subscribe(
      () => {
        this.snackBar.open('Login was successful. Welcome.');
        this.router.navigateByUrl('/tasks');
      },
      () => this.snackBar.open('Username and/or Password is wrong. Please try again.')
    ));
  }

  ngOnDestroy(): void {
    this.subs.unsubscribe();
  }
}
