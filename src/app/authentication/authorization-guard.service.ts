import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree} from '@angular/router';
import {Observable, of, throwError} from 'rxjs';
import {AuthenticationService} from "./service/authentication.service";
import {User} from "./model/user";
import {catchError, map} from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class AuthorizationGuard implements CanActivate {

  constructor(
    private authenticationService: AuthenticationService,
    private router: Router
  ) {
  }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return this.authenticationService.user$.pipe(
      map(currentUser => {
        if (currentUser?.permissions?.includes('ADMIN_PANEL')) {
          return true;
        } else {
          throw new Error('Valid token not returned')
        }
      }),
      catchError(() => {
          this.router.navigate(['login'], {replaceUrl: true});
          return of(false);
        }
      ));
  }
}
