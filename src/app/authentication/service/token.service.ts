import {Injectable} from '@angular/core';
import jwt_decode from "jwt-decode";

@Injectable({
  providedIn: 'root'
})
export class TokenService {

  constructor() {
  }

  getToken(): string | null {
    return localStorage.getItem('token');
  }

  setToken(token: string): void {
    localStorage.setItem('token', token);
  }

  deleteToken(): void {
    localStorage.removeItem('token');
  }

  getUserIdFromToken(): string | undefined {
    // TODO: handle exception on decoding error. preferably api should return user by token, no need for userId
    const token = this.getToken();
    return (token) ? jwt_decode<{ sub: string }>(token).sub : undefined;
  }
}
