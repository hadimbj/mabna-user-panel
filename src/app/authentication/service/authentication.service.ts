import {Injectable} from '@angular/core';
import {User} from "../model/user";
import {ApiService} from "../../core/service/api.service";
import {first, map, switchMap, tap} from "rxjs/operators";
import {TokenService} from "./token.service";
import {AuthenticationRS} from "../model/authentication-rs";
import {Observable, of, ReplaySubject, throwError} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  user$ = new ReplaySubject<User>(1);
  private emailTailString = '@example.com';

  constructor(
    private api: ApiService,
    private tokenService: TokenService
  ) {
    this.checkAuthenticationStatus();
  }

  registerUser(user: Partial<User>): Observable<User> {
    user.email += this.emailTailString;
    return this.api.post<AuthenticationRS>('register', user).pipe(
      tap(authRS => this.tokenService.setToken(authRS.accessToken)),
      switchMap(() => this.getUser()),
    )
  }

  LoginUser(email: string, password: string): Observable<User> {
    email += this.emailTailString;
    return this.api.post<AuthenticationRS>('login', {email, password}).pipe(
      tap(authRS => this.tokenService.setToken(authRS.accessToken)),
      switchMap(() => this.getUser()),
    )
  }

  logoutUser(): void {
    this.tokenService.deleteToken();
    this.user$.next(undefined);
  }

  getUser(): Observable<User> {
    return this.api.get<User>(`users/${this.tokenService.getUserIdFromToken()}`).pipe(
      tap(user => this.user$.next(user))
    );
  }

  private checkAuthenticationStatus() {
    if (this.tokenService.getToken()) {
      this.getUser().pipe(first()).subscribe({
        error: () => this.user$.next(undefined)
      });
    }
  }

  isLoggedIn(): Observable<boolean> {
    if (!this.tokenService.getToken()) {
      return throwError('No token was found');
    } else {
      return this.user$.pipe(map(user => !!user))
    }
  }
}
