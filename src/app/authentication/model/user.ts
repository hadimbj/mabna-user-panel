export interface User {
  id: string;
  name: string;
  email: string;
  password: string;
  phoneNumber: string;
  permissions?: UserPermissions[];
}

export type UserPermissions = 'ADMIN_PANEL';
