import {Component, HostListener, OnInit} from '@angular/core';
import {AuthenticationService} from "../../authentication/service/authentication.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-back-office',
  templateUrl: './back-office.component.html',
  styleUrls: ['./back-office.component.scss']
})
export class BackOfficeComponent implements OnInit {
  isMenuVisible = true;
  isMobile: boolean;

  @HostListener('window:resize', ['$event'])
  sizeChange() {
    this.isMobile = window.innerWidth < 460;
  }

  constructor(
    private authenticationService: AuthenticationService,
    private router: Router
  ) {
    this.isMobile = window.innerWidth < 460;
  }

  ngOnInit(): void {
  }

  onHideMenu() {

  }

  toggleMenu() {
    this.isMenuVisible = !this.isMenuVisible;
  }

  logout() {
    this.authenticationService.logoutUser();
    this.router.navigateByUrl('/');
  }
}
