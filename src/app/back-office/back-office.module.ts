import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {BackOfficeRoutingModule} from './back-office-routing.module';
import {BackOfficeComponent} from './back-office/back-office.component';
import {DashboardComponent} from './dashboard/dashboard.component';
import {MatSidenavModule} from "@angular/material/sidenav";
import {MatButtonModule} from "@angular/material/button";
import {MatToolbarModule} from "@angular/material/toolbar";
import {MatIconModule} from "@angular/material/icon";
import {MatListModule} from "@angular/material/list";
import {MatTooltipModule} from "@angular/material/tooltip";


@NgModule({
  declarations: [
    BackOfficeComponent,
    DashboardComponent
  ],
  imports: [
    CommonModule,
    BackOfficeRoutingModule,
    MatSidenavModule,
    MatButtonModule,
    MatToolbarModule,
    MatIconModule,
    MatListModule,
    MatTooltipModule
  ]
})
export class BackOfficeModule {
}
