<a name="0.1.0"></a>
# 0.1.0 (2021-07-15)

### Features

* **core:** implement core module which contains basic elements required to run app
* **core:** implement api service containing (get / put / post / delete) methods
* **core:** add HttpParamsObject type
* **core:** implement error handler service. right now the service is a simple console.log, it will be implemented with better  and use of log service in future versions.
* **lazy loading:** implement lazy loading and add AppRouting.Module
* **lint:** add eslint
* **dependencies:** add angular material
* **dependencies:** add tailwindcss

